import os
import json
from glob import glob
from uuid import uuid4

from package.api.constants import NOTES_DIR


class Note:
    def __init__(self, title="", content="", uuid=None):
        if uuid:
            self.uuid = uuid
        else:
            self.uuid = str(uuid4())

        self.title = title
        self.content = content

    def __repr__(self):
        return f"{self.title} [{self.uuid}]"

    def __str__(self):
        return self.title

    @property
    def path(self):
        return os.path.join(NOTES_DIR, self.uuid + ".json")

    @property
    def content(self):
        return self._content
    
    @content.setter
    def content(self, value):
        if isinstance(value, str):
            self._content = value
        else:
            raise TypeError("Le format n'est pas une chaîne de caratère!")

    def save(self):
        if not os.path.exists(NOTES_DIR):
            os.makedirs(NOTES_DIR)

        dictionary = {"title": self.title, "content": self.content}

        with open(self.path, "w") as file:
            json.dump(dictionary, file, indent=4)

    def delete(self):
        os.remove(self.path)
        if os.path.exists(self.path):
            return False
        else:
            return True


def get_notes():
    notes=[]
    files = glob(os.path.join(NOTES_DIR, "*.json"))
    for file in files:
        with open(file, "r") as current_file:
            data_ = json.load(current_file)
            uuid_ = os.path.splitext(os.path.basename(file))[0]
            title_ = data_.get("title")
            content_ = data_.get("content")
            note_ = Note(uuid=uuid_, title=title_, content=content_)
            notes.append(note_)

    return notes
