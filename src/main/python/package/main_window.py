from PySide2 import QtWidgets, QtGui

from package.api.note_api import Note
from package.api.note_api import get_notes


class MainWindow(QtWidgets.QWidget):
    def __init__(self, application_context):
        super().__init__()
        self.app_ctx = application_context
        self.setWindowTitle("iNotes")
        self.setup_ui()
        self.load_notes()

    def setup_ui(self):
        self.create_widgets()
        self.create_layouts()
        self.modify_widgets()
        self.add_widgets_to_layouts()
        self.setup_connections()

    def create_widgets(self):
        self.btn_create_note = QtWidgets.QPushButton("Nouvelle note")
        self.lw_note = QtWidgets.QListWidget()
        self.te_contenu = QtWidgets.QTextEdit()

    def modify_widgets(self):
        style_file = self.app_ctx.get_resource("style.css")
        with open(style_file, "r") as file:
            self.setStyleSheet(file.read())

    def create_layouts(self):
        self.main_layout = QtWidgets.QGridLayout(self)

    def add_widgets_to_layouts(self):
        self.main_layout.addWidget(self.btn_create_note, 0, 0, 1, 1)
        self.main_layout.addWidget(self.lw_note, 1, 0, 1, 1)
        self.main_layout.addWidget(self.te_contenu, 0, 1, 2, 1)

    def setup_connections(self):
        self.btn_create_note.clicked.connect(self.create_note)
        self.te_contenu.textChanged.connect(self.save_note)
        self.lw_note.itemSelectionChanged.connect(self.load_note_content)
        QtWidgets.QShortcut(QtGui.QKeySequence("Backspace"), self.lw_note, self.delete_selected_note)

    def add_note_to_list_widget(self, note):
        lw_item = QtWidgets.QListWidgetItem(note.title)
        lw_item.note = note
        self.lw_note.addItem(lw_item)

    def create_note(self):
        title_, result_ = QtWidgets.QInputDialog.getText(self, "Ajouter une note", "Titre: ")
        if result_ and title_:
            note = Note(title=title_)
            note.save()
            self.add_note_to_list_widget(note)

    def get_selected_lw_item(self):
        selected_items = self.lw_note.selectedItems()
        if selected_items:
            return selected_items[0]
        else:
            return None

    def delete_selected_note(self):
        selected_item = self.get_selected_lw_item()
        if selected_item:
            deleted = selected_item.note.delete()
            if deleted:
                self.lw_note.takeItem(self.lw_note.row(selected_item))

    def load_notes(self):
        notes = get_notes()
        for note in notes:
            self.add_note_to_list_widget(note)

    def load_note_content(self):
        selected_item = self.get_selected_lw_item()
        if selected_item:
            self.te_contenu.setText(selected_item.note.content)
        else:
            self.te_contenu.clear()

    def save_note(self):
        selected_item = self.get_selected_lw_item()
        if selected_item:
            selected_item.note.content = self.te_contenu.toPlainText()
            selected_item.note.save()
